from modules.output import Output

class UniversalWordVector(Output):
    def __init__(self, output_file):
        super().__init__(output_file)

    def expand(self, rootwords):
        for key, value in rootwords.items():
            if key in self.dataframe[self.COLUMN_1_HEADER].values:
                self.dataframe.at[self.dataframe[self.COLUMN_1_HEADER] == key ,self.COLUMN_2_HEADER] = self.dataframe[self.dataframe[self.COLUMN_1_HEADER] == key][self.COLUMN_2_HEADER] + value
            else:
                self.update([key, value])
    
    def export(self):
        self.dataframe = self.dataframe.sort_values(by = [self.COLUMN_1_HEADER])
        super().export()
	# def purge(self):
	# 	for key in list(self.vector):
	# 		if self.vector[key] < 5:
	# 			del self.vector[key]