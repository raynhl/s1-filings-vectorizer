import pandas as pd
import logging
import os

logger = logging.getLogger()

class Output:
    COLUMN_1_HEADER = "Word"
    COLUMN_2_HEADER = "Count"

    def __init__(self, output_file):
        self.output_file = output_file
        logger.info("[Output file] {}".format(self.output_file))

        self.dataframe = pd.DataFrame(
                columns=[
                    self.COLUMN_1_HEADER,
                    self.COLUMN_2_HEADER])
    
    def update(self, data):
        # logger.info(data)
        new_dataframe = pd.DataFrame(
                [data],
                columns=[
                    self.COLUMN_1_HEADER,
                    self.COLUMN_2_HEADER])

        self.dataframe = self.dataframe.append(new_dataframe, ignore_index= True)

    def export(self):
        # if (self.word_dump):
        #     words = self.dataframe.to_string(
        #                 columns=[self.COLUMN_1_HEADER],
        #                 header = False,
        #                 index = False)

        #     words = words.replace('\n', ',')

        #     with open(self.output_file, "w") as file:
        #         logger.info("Exporting word dump to txt file: {}".format(self.output_file))
        #         file.write(words)
            
        # else:
        with pd.ExcelWriter(self.output_file) as writer:
            logger.info("Exporting data to Excel file: {}".format(self.output_file))
            self.dataframe.to_excel(writer, sheet_name = "Results", index = False)
