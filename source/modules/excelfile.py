import pandas as pd

class ExcelFile:
    def __init__(self, filepath, sheets = 0):
        self.filepath = filepath
        self.sheets = sheets

        try:
            if "csv" in self.filepath:
                self.dataframe = pd.read_csv(self.filepath)
            else:
                self.dataframe = pd.read_excel(self.filepath, self.sheets)

            self.ready = True
        except:
            self.ready = False

    def head(self, count = 5):
        if self.ready: print(self.dataframe.head(count))
    

