import asyncio
from bs4 import BeautifulSoup
from aiohttp import ClientSession
import unicodedata
import logging
import os

logger = logging.getLogger()

class Downloader():
    def __init__(self, urls):
        self.urls = urls
        self.exportable = os.environ["SCRAPER_DUMP"] == "on"
        self.dump_file = str(os.environ["DUMP_FILE"])

        if (self.exportable):
            print("\nDownloaded contents will be exported to {}".format(self.dump_file))
            logger.info("Downloaded contents will be exported to {}".format(self.dump_file))

        self.results = asyncio.run(self.fetch_urls())
        self.documents = self.get_texts()

    def get_results(self):
        return self.results

    def get_documents(self):
        return self.documents

    async def fetch_urls(self):
        tasks = []
        responses = None
        async with ClientSession() as session:
            for url in self.urls:
                tasks.append(asyncio.create_task(self.fetch_url(session, url)))
        
            responses = await asyncio.gather(*tasks)
        
        return responses

    async def fetch_url(self, session, url):
        logger.info("Downloading: {}".format(url))
        try:
            async with session.get(url) as response:
                return await response.read()
        except:
            logger.warning("Unable to download: {}".format(url))
            return None

    def get_texts(self):
        documents = []
        for result in self.results:
            if not result:
                documents.append("")
                continue

            soup = BeautifulSoup(result, "html.parser")
            documents.append(unicodedata.normalize("NFKD", soup.get_text()).strip())

        return documents
    
    def export(self):
        if not self.exportable:
            return
        
        with open(self.dump_file, "a+") as dump_file:
            dump_file.write(' '.join(map(str, self.documents)))