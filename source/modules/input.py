from modules.excelfile import ExcelFile
import numpy as np
import logging

logger = logging.getLogger()

class Input(ExcelFile):
    COMPANY_HEADER = "Company"
    S1_URL = "S-1 URL"

    def __init__(self, input_file):
        super().__init__(input_file)
        logger.info("[Input file] {}".format(input_file))
    
    def get_row(self):
        for index, row in self.dataframe.iterrows():
            yield index, [row[self.COMPANY_HEADER],
                    row[self.S1_URL]
            ]

    def get_urls(self, chunk_size):
        logger.info("Splitting into {} URL groups".format(chunk_size))
        for item in np.array_split(self.dataframe, chunk_size):
            yield item[self.S1_URL].tolist()