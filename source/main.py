from modules.language_processor import LanguageProcessor
from modules.input import Input
from modules.downloader import Downloader
from modules.universal_word_vector import UniversalWordVector

import time
import sys
import logging as logger
import progressbar

logger.basicConfig(filename='log/s1-filings-vectorizer.log', level=logger.INFO, filemode='w', format='[%(levelname)s] %(module)s:%(funcName)s - %(message)s')

input_file_path = str(sys.argv[1])
output_file_path = str(sys.argv[2])
number_of_chunks = int(sys.argv[3])

def main():
    input = Input(input_file_path)
    word_vector_output = UniversalWordVector(output_file_path)
    input_generator = input.get_urls(number_of_chunks)
    
    
    print("Processing URLs: ")
    with progressbar.ProgressBar(max_value = number_of_chunks) as bar:
        for index, urls in enumerate(input_generator):
            bar.update(index + 1)
            try:
                if not urls:
                    bar.update(number_of_chunks)
                    return

                downloader = Downloader(urls)

                for document in downloader.get_documents():
                    language_processor = LanguageProcessor(str(document))
                    word_vector_output.expand(language_processor.rootwords)
                
                downloader.export()

            except KeyboardInterrupt:
                print("[EXITING] Ctrl-C pressed. Preparing to exit program now.")
                logger.info("[EXITING] Ctrl-C pressed. Preparing to exit program now.")
                word_vector_output.export()
                sys.exit(0)

            except Exception as e:
                logger.exception(e)
                logger.warning("Failed to process chunk #{}".format(index + 1))

            finally:
                print("\nSaving word vector checkpoint to {}".format(output_file_path))     
                word_vector_output.export()

if __name__ == "__main__":
    with open("version") as f:
        version = f.read()

    print("Starting main program. Version is v{}".format(version))
    logger.info("Starting main program. Version is v{}".format(version))
    print("Input file: {}".format(input_file_path))
    print("Output file: {}".format(output_file_path))
    main()
    print("Program. Exiting.")