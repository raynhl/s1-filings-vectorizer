#!/bin/bash

export DOCKER_PATH=/usr/src/s1_filings_vectorizer
export SCRAPER_DUMP="off"

if [ "$#" -lt 2 ]; then
    echo "Usage: $0 <input_file> <output_file> [dump]"
	exit 1
fi

if [ ! -f `pwd`/$1 ]; then
	echo "$1 is not found in `pwd`/input/"
	echo "Please refer to `pwd`/input/s-1-sheet.xlsx for input file example."
	exit 1
fi

if [ "$3" == "dump" ]; then
	export SCRAPER_DUMP="on"
	export DUMP_FILE=`echo "$2" | sed -e 's/xlsx/dump/g'` 
fi

sudo docker run -e SCRAPER_DUMP=$SCRAPER_DUMP -e DUMP_FILE=$DUMP_FILE -v `pwd`/input/:$DOCKER_PATH/input -v `pwd`/output/:$DOCKER_PATH/output/ -v `pwd`/log/:$DOCKER_PATH/log/ -it --rm raynhl/s1-filings-vectorizer:2.0 $1 $2 200