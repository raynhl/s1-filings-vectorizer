#!/bin/bash

sudo apt-get install -y curl
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install -y docker-ce
sudo groupadd docker
sudo gpasswd -a $USER docker
sudo usermod -aG docker ${USER}
sudo service docker restart

mkdir -p `pwd`/input
mkdir -p `pwd`/output
mkdir -p `pwd`/log

echo "Set up is complete. You may now start using the scraping program."
